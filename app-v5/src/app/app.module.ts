import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//import material here plz
import { MaterialModule } from './material/material.module';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

//costum module and component
import { LoginModule } from './login/login.module';
import { AboutUsModule } from './about-us/about-us.module';
import { EventsModule } from './events/events.module';
import { ProfilModule } from './profil/profil.module';
import { PokedexModule } from './pokedex/pokedex.module';
import { SignInModule } from './sign-in/sign-in.module';
import { InfoBoxModule } from './accueil/info-box.module';

//miscellaneous imports
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { PostComponent } from './post/post.component';


@NgModule({
  declarations: [
    AppComponent,
    PostComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    LoginModule,
    AboutUsModule,
    EventsModule,
    ProfilModule,
    PokedexModule,
    InfoBoxModule,
    SignInModule,
    HttpClientModule, AngularSvgIconModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
